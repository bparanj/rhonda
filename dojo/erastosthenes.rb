class Erastosthenes 
  def initialize(n)
    @n = n
    @list = number_list
  end
      
  def calculate
    list = number_list
    list.each do |x|
      unless x >= Math.sqrt(@n)
        cross_out_multiples_of(x)
      end
    end
    @list
  end
  
  def self.next(n)
    e = Erastostenes.new(100)  
    primes = e.calculate
    primes.detect{|x| x > n}
  end
  
  private
  
  def cross_out_multiples_of(number)
    @list.reject! do |x|
      unless x == number
        x % number == 0 
      end
    end    
  end
  
  def number_list
    (2..@n).to_a  
  end
  
end
