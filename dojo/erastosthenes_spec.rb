require_relative 'erastosthenes'

describe Erastosthenes do
    
  it 'should calculate the prime numbers for 4' do
    e = Erastosthenes.new(4)
    result = e.calculate
        
    expect(result).to eq([2,3,4])    
  end
  
  it 'should calculate the prime numbers for 30' do
    e = Erastosthenes.new(30)
    result = e.calculate
        
    expect(result).to eq([2,3,5,7,11,13,17,19,23,29])
  end
  
end