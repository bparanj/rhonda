class Exchange
  attr_reader :x, :y
  
  def initialize(x, y)
    @x = x
    @y = y
  end
  
  def perform
    t = @x
    @x = @y
    @y = t
  end
end
  

describe Exchange do
  it 'should swap the values of the given two variables' do
    e = Exchange.new(1, 2)
    e.perform
    
    expect(e.x).to eq(2)
  end
end