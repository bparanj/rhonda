class FindPrime
  LOWER_BOUND = 2
  
  def initialize(n)
    @n = n
  end
  
  # 1. Get the list of factors to use for the modulo
  # 2. Find the modulo using the first number
  # 3. Repeat modulo for all numbers
  # 4. If all the numbers modulo returns remainder, then return true  
  def prime?
    divisors_list.each do |e|
      remainder = @n % e
       return false if remainder == 0
    end
    return true
  end
    
  private
  
  def divisors_list
    (LOWER_BOUND..upper_factor).to_a
  end
  
  def upper_factor
    Math.sqrt(@n).ceil  
  end
  
end

describe FindPrime do
    
  it 'should return true given a number input of 37' do
    find_prime = FindPrime.new(37)
    
    is_prime = find_prime.prime?
    expect(is_prime).to eq(true)
  end
  
end