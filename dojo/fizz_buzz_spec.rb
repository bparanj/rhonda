require_relative 'fizz_buzz'

describe FizzBuzz do
  
  it 'should replace multiples of 3 with Fizz' do
    game = FizzBuzz.new
    
    result = game.sequence
    
    expect(result[2]).to eq('Fizz')
  end
  
  it 'should replace multiples of 5 with Buzz' do
    game = FizzBuzz.new
    
    result = game.sequence
    
    expect(result[4]).to eq('Buzz')
  end
  
  it 'should replace multiples of both 3 and 5 with FizzBuzz' do
    game = FizzBuzz.new
    
    result = game.sequence
    
    expect(result[14]).to eq('FizzBuzz')
  end
  
end