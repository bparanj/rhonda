
describe 'Float accuracy' do
  it 'should be within a given decimal places' do
    x = 139.25
    y = 74.79
    
    expect(x + y).to be_within(0.001).of(214.04)
  end  
end