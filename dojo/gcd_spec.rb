class Gcd
  def initialize(x, y)
    @x = x
    @y = y
    initialize_numbers
  end
  
  def initialize_numbers
    if @x > @y 
      @bigger_number = @x
      @smaller_number = @y
    else
      @bigger_number = @y
      @smaller_number = @x      
    end
  end
  
  def calculate
    remainder = 1
    divident = @bigger_number
    divisor = @smaller_number  
    until remainder == 0
      remainder = divident % divisor
      divident = divisor
      divisor = remainder
    end
    divident
  end
  
end


describe 'GCD' do  
  it 'should return 4 for 8 and 12' do
    gcd = Gcd.new(8,12)
    result = gcd.calculate
    
    expect(result).to eq(4)    
  end
  
  it 'should return 6 for 54 and 24' do
    gcd = Gcd.new(24,54)
    result = gcd.calculate
    
    expect(result).to eq(6)    
  end
  
  it 'should return 6 for 12,30' do
    gcd = Gcd.new(12,30)
    result = gcd.calculate

    expect(result).to eq(6)
  end

end