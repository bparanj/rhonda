class MyCount
  def calculate(n, a)
    count = 0
    index = 0
    
    while index < a.size
      if a[index] >= n
        count += 1
      end
      index += 1
    end
    count
  end
end

describe MyCount do
  
  it "should return 0 for n = 0 and an empty list" do
    my_count = MyCount.new
    
    result = my_count.calculate(0, [])
    
    expect(result).to eql(0)
  end
  
  it "should return 1 for 0, [0]" do
    my_count = MyCount.new
    
    result = my_count.calculate(0, [0])
    
    expect(result).to eql(1)
  end
  
  it "should return 0 for 1, [0]" do
    my_count = MyCount.new
    
    result = my_count.calculate(1, [0])
    
    expect(result).to eql(0)
  end
  
  it "should return 2 for 1, [1,1]" do
    my_count = MyCount.new
    
    result = my_count.calculate(1, [1,1])
    
    expect(result).to eql(2)  
  end
  
  it "should return 5 for 3, [1,1,2,3,4,3,6,6,1]" do
    my_count = MyCount.new
    
    result = my_count.calculate(3, [1,1,2,3,4,3,6,6,1])
    
    expect(result).to eql(5)      
  end
end