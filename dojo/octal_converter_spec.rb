class OctalConverter
  def initialize(number)
    @number = number
  end
  
  def result
    octal = []
    until finished?
      digit = extract_octal_digit
      reduce
        
      octal.unshift(digit)
    end
      
    octal
  end
  
  private
  
  def extract_octal_digit
    @number % 8
  end
  
  def reduce
    @number = @number / 8 
  end
  
  def finished?
    @number == 0
  end
end


describe OctalConverter do
  it 'should return 1 for 1' do
    converter = OctalConverter.new(1)
    
    expect(converter.result).to eq([1])
  end

  it 'should return 2 for 2' do
    converter = OctalConverter.new(2)
    
    expect(converter.result).to eq([2])
  end
  
  it 'should return 10 for 8' do
    converter = OctalConverter.new(8)
    
    expect(converter.result).to eq([1,0])    
  end
  
  it 'should return 20 for 16' do
    converter = OctalConverter.new(16)
    
    expect(converter.result).to eq([2,0])    
  end
  
  it 'should return 137 for 95' do
    converter = OctalConverter.new(95)
    
    expect(converter.result).to eq([1, 3, 7])    
  end
  
  it 'should return 4000 for 2048' do
    converter = OctalConverter.new(2048)
    
    expect(converter.result).to eq([4, 0, 0, 0])    
  end
  
end