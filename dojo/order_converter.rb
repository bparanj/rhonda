class Order
  attr_reader :amount, :number
  
  def initialize(amount, number)
    @amount = amount
    @number = number
  end
end

class OrderXmlConverter
  def initialize(order)
    @order = order
  end
  def convert
    "<order><amount>#{@order.amount}</amount><number>#{@order.number}</number></order>"
  end
end

# order = Order.new(19, 2)
# format = 'Xml'
# class_name = Object.const_get("Order#{format}Converter")
# converter = class_name.new(order)
# puts converter.convert
#
# In rails :
# class_name = "Order#{format}Converter".constantize
