require_relative 'erastosthenes.rb'
require 'prime'

class PrimeFactor
  def initialize(n)
    @n = n
  end
    
  def calculate
    result = []
    
    current_prime = 2
    
    until Prime.prime?(@n)
      if (@n % current_prime) == 0
        result << current_prime
        @n = @n / current_prime
      else
        current_prime = Erastostenes.next(current_prime)
      end
    end
    result << @n
        
    result
  end
end

describe PrimeFactor do
  it 'should return 2 for input of 2' do
    prime_factorial = PrimeFactor.new(2)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([2])
  end
  
  it 'should return 3 for input of 3' do
    prime_factorial = PrimeFactor.new(3)

    prime = prime_factorial.calculate

    expect(prime).to eq([3])
  end

  it 'should return [2,2] for input of 4' do
    prime_factorial = PrimeFactor.new(4)

    prime = prime_factorial.calculate

    expect(prime).to eq([2,2])
  end

  it 'should return [2, 2, 3] for input of 12' do
    prime_factorial = PrimeFactor.new(12)

    prime = prime_factorial.calculate

    expect(prime).to eq([2,2,3])
  end

  it 'should return [3,7,7] for input of 147' do
    prime_factorial = PrimeFactor.new(147)

    prime = prime_factorial.calculate

    expect(prime).to eq([3,7,7])    
  end

  it 'should return [2,3] for input of 6' do
    prime_factorial = PrimeFactor.new(6)

    prime = prime_factorial.calculate

    expect(prime).to eq([2, 3])
  end
  
  it 'should return [2,2, 2] for input of 8' do
    prime_factorial = PrimeFactor.new(8)

    prime = prime_factorial.calculate

    expect(prime).to eq([2, 2, 2])
  end
  
  it 'should return [2,7] for input of 14' do
    prime_factorial = PrimeFactor.new(14)

    prime = prime_factorial.calculate

    expect(prime).to eq([2, 7])
  end

  it 'should handle any number' do
    prime_factorial = PrimeFactor.new(168)

    prime = prime_factorial.calculate

    expect(prime).to eq([2, 2, 2, 3, 7])
  end
  
  it 'should handle any number' do
    prime_factorial = PrimeFactor.new(330)

    prime = prime_factorial.calculate

    expect(prime).to eq([2, 3, 5, 11])
  end
  
end