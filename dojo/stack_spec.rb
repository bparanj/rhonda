class Stack
  def initialize
	  @elements = []
  end
	
  def push(item)
	  @elements << item
  end
	
  def	pop
	  @elements.pop
  end
	
  def size
	  @elements.size
  end
  
  def empty?
    @elements.empty?
  end
  
  def peek
    @elements.last
  end
end

describe Stack do
  
  it 'if one pushes x then peeks, the value returned is x, but the size stays the same' do
    stack = Stack.new
    stack.push(1)
    
    expect(stack.peek).to eq(1)
    expect(stack.size).to eq(1)
  end
  
  it 'after n pushes to an empty stack, n > 0, the stack is not empty' do
    stack = Stack.new
    stack.push(1)
    stack.push(2)
    
    expect(stack.empty?).to be(false)
  end

  it 'after n pushes to an empty stack, n > 0, its size is n' do
    stack = Stack.new
    stack.push(1)
    stack.push(2)
    
    expect(stack.size).to eq(2)
  end
  
  it 'should be empty on construction' do
    stack = Stack.new
    
    expect(stack.empty?).to be(true)
  end
  
  it 'should push a given item' do
    stack = Stack.new
	
    stack.push(1)
	
    expect(stack.pop).to eq(1)
  end 

  it 'should pop from the stack' do
    stack = Stack.new
    stack.push(2)
	
    result = stack.pop
	
    expect(result).to eq(2)
    expect(stack.size).to eq(0)
  end
  
  it 'If the size is n, then after n pops, the stack is empty and has a size 0' do
    stack = Stack.new
    stack.push(1)
    stack.push(2)
    
    stack.pop
    stack.pop
    
    expect(stack.empty?).to be(true)
    expect(stack.size).to eq(0)
  end
end

