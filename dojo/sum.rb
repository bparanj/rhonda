class Sum

  def calculate(numbers)
    index = 0
    result = 0
    
    while index < numbers.size
     result += numbers[index]
     index += 1
    end
    result
  end

end