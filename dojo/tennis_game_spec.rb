class Player
  attr_reader :points, :name
  
  def initialize(name='')
    @name = name
    @points = 0
  end  
  
  def wins_point
    @points += 1
  end
  
  def score
    if @points == 0
      'Love'
    elsif @points == 1
      'Fifteen'
    elsif @points == 2
      'Thirty'
    elsif @points == 3
      'Forty'
    end
  end
end

class TennisGame  
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two  
  end
  
  def wins_point(player)
    @player_one.wins_point
  end
  
  def player_one_score
    @player_one.score
  end
  
  def player_two_score
    @player_two.score
  end
  
  def score
    if @player_one.points == 3 and @player_two.points == 3
      'Duece'
    elsif (@player_one.points == @player_two.points + 1) and (@player_two.points >=3)
      "Advantage #{@player_one.name}"
    elsif (@player_one.points >= 4) and (@player_one.points >= (@player_two.points + 2))
      "Game #{@player_one.name}"
    end
  end
end

describe TennisGame do
  it 'initial scores for both players must be Love' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    
    player_one_score = game.player_one_score
    player_two_score = game.player_two_score
    
    expect(player_one_score).to eq('Love')
    expect(player_two_score).to eq('Love')
  end
  
  it 'returns 15 for player one when player one wins the point' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    
    player_one_score = game.player_one_score
    player_two_score = game.player_two_score
    
    expect(player_one_score).to eq('Fifteen')
    expect(player_two_score).to eq('Love')
  end
  
  it 'returns 30 for player one when player one wins two points' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_one.wins_point
    
    player_one_score = game.player_one_score
    player_two_score = game.player_two_score
    
    expect(player_one_score).to eq('Thirty')
    expect(player_two_score).to eq('Love')    
  end
  
  it 'returns 40 for player one when player one wins three points' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_one.wins_point
    player_one.wins_point
    
    player_one_score = game.player_one_score
    player_two_score = game.player_two_score
    
    expect(player_one_score).to eq('Forty')
    expect(player_two_score).to eq('Love')    
  end
  
  it 'returns 15 for both players when both have won one point' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_two.wins_point
    
    player_one_score = game.player_one_score
    player_two_score = game.player_two_score
    
    expect(player_one_score).to eq('Fifteen')
    expect(player_two_score).to eq('Fifteen')        
  end
  
  it 'returns duece both players have won three points' do
    player_one = Player.new
    player_two = Player.new
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_one.wins_point
    player_one.wins_point
    
    player_two.wins_point
    player_two.wins_point
    player_two.wins_point
      
    expect(game.score).to eq('Duece')
  end
  
  it 'returns Advantage for the player with one more point after 3 points by both' do

    player_one = Player.new('McEnroe')
    player_two = Player.new('Borg')
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_one.wins_point
    player_one.wins_point
    
    player_two.wins_point
    player_two.wins_point
    player_two.wins_point

    player_one.wins_point
    
    expect(game.score).to eq("Advantage #{player_one.name}")
  end 


  it "Game 'winner name' when a player wins 4 points with no points by the opponent" do

    player_one = Player.new('McEnroe')
    player_two = Player.new('Borg')
    
    game = TennisGame.new(player_one, player_two)
    player_one.wins_point
    player_one.wins_point
    player_one.wins_point
    player_one.wins_point
    
    expect(game.score).to eq("Game #{player_one.name}")
  end 
  
end