class FindPrime
  LOWER_BOUND = 2
  attr_accessor :upper_factor
  
  def initialize(n)
    @n = n
    @upper_factor = upper_bound(n)
  end

  # 1. Get the list of factors to use for the modulo
  # 2. Find the modulo using the first number
  # 3. Repeat modulo for all numbers
  # 4. If all the numbers modulo returns remainder, then return true  
  def prime?
    upper_factor_list.each do |e|
      remainder = @n % e
       return false if remainder == 0
    end
    return true
  end
      
  private
  
  def upper_bound(number)
    Math.sqrt(number).ceil
  end
  
  def upper_factor_list
    (LOWER_BOUND..@upper_factor).to_a
  end
  
end
