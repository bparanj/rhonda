# 2
class FindsEqualGreater
  def count(num, array)
    index = 0
    count = 0
    
    while index < array.size
      count += 1 if array[index] >= num
      index += 1
    end

    count
  end
end