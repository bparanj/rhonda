require 'prime'

class PrimeFactorial
  DIVISIOR = 2
  
  def initialize(number)
    @number = number
    @result = []
  end
  
  def calculate
    if prime?(@number) 
      @result = [@number] 
    else      
      until finished?
        remainder = @number / 2
        new_number = remainder

        if prime?(new_number)
          @result << new_number
        end
        if (new_number / 2) == 1
          @result << new_number 
        else
          @result << DIVISIOR
        end        
      end
    end
    @result
  end
  
  private
  
  def prime?(remainder)
    Prime.prime?(remainder)
  end
  
  def finished?
    (@result.inject(){|r, e| r *= e}).to_i >= @number
  end
end
