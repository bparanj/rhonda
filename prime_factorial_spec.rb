require_relative 'prime_factorial'

describe 'PrimeFactorial' do
  it 'should return 2 for input of 2' do
    prime_factorial = PrimeFactorial.new(2)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([2])
  end
  
  it 'should return 3 for input of 3' do
    prime_factorial = PrimeFactorial.new(3)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([3])
  end
  
  it 'should return [2,2] for input of 4' do
    prime_factorial = PrimeFactorial.new(4)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([2,2])
  end
  
  xit 'should return [2,3] for input of 6' do
    prime_factorial = PrimeFactorial.new(6)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([2, 3])    
  end
  
  it 'should return [2,2, 2] for input of 8' do
    prime_factorial = PrimeFactorial.new(8)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([2, 2, 2])    
  end

  it 'should return [2,7] for input of 14' do
    prime_factorial = PrimeFactorial.new(14)
    
    prime = prime_factorial.calculate
    
    expect(prime).to eq([7, 2])    
  end
  # it 'should handle any number' do
  #   prime_factorial = PrimeFactorial.new(2*3*4*7)
  #   
  #   prime = prime_factorial.calculate
  #   
  #   expect(prime).to eq([2, 3, 4, 7])    
  # end
end