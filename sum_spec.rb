require_relative 'sum'

describe Sum do
  it 'should return 0 for empty list' do
    s = Sum.new
    result = s.calculate([])
    
    expect(result).to eq(0)
  end
  
  it 'should return 0 for a list containing 0' do
    s = Sum.new
    result = s.calculate([0])
    
    expect(result).to eq(0)
  end
  
  it 'should return 1 for a list containing 1' do
    s = Sum.new
    result = s.calculate([1])
    
    expect(result).to eq(1)
  end
  
  it 'should return 2 for a list containing 1 and 1' do
    s = Sum.new
    result = s.calculate([1,1])
    
    expect(result).to eq(2)
  end
  
  it 'should return 20 for a list containing 2,2,2,2,2,2,2,2,2 and 2' do
    s = Sum.new
    result = s.calculate([2,2,2,2,2,2,2,2,2,2])
    
    expect(result).to eq(20)
  end
end